﻿using BaseVersion.Helpers;
using System.Web.Optimization;

namespace BaseVersion
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            if (CultureHelper.IsRighToLeft())
            {
                bundles.Add(new ScriptBundle("~/bundles/dashboard_global").Include(
                       "~/Content/js/jquery.min.js",
                       "~/Content/js/bootstrap.min.js",
                       "~/Content/js/slimscroll/jquery.slimscroll.min.js",
                       "~/Content/components/msgGrowl/js/msgGrowl.js",
                       "~/Content/components/zoomify/zoomify.js",
                       "~/Content/components/bootstrap-select/js/bootstrap-select.js",
                       "~/Content/js/skins.min.js",
                       "~/Content/js/datetime/bootstrap-datepicker-ar.js",
                       "~/Content/js/select2/select2-ar.js"
                       ));

                bundles.Add(new StyleBundle("~/Content/dashboard_global")
                    .Include("~/Content/css/bootstrap.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/css/font-awesome.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/components/msgGrowl/css/msgGrowl.css", new CssRewriteUrlTransform())
                    .Include("~/Content/css/custom-rtl.css", new CssRewriteUrlTransform())
                    .Include(
                    "~/Content/css/my_dashboard.css",
                    "~/Content/css/demo.min.css",
                    "~/Content/css/animate.min.css",
                    "~/Content/components/zoomify/zoomify.css",
                    "~/Content/components/bootstrap-select/css/bootstrap-select.css",
                    "~/Content/css/bootstrap-rtl.min.css",
                    "~/Content/css/beyond-rtl.min.css",
                    "~/Content/css/4095-rtl.min.css"
                    ));

            }
            else
            {
                bundles.Add(new ScriptBundle("~/bundles/dashboard").Include(
                       "~/Content/js/jquery.min.js",
                       "~/Content/js/bootstrap.min.js",
                       "~/Content/js/slimscroll/jquery.slimscroll.min.js",
                       "~/Content/components/msgGrowl/js/msgGrowl.js",
                       "~/Content/components/zoomify/zoomify.js",
                       "~/Content/components/bootstrap-select/js/bootstrap-select.js",
                       "~/Content/js/select2/select2.js",
                       "~/Content/js/datetime/bootstrap-datepicker.js"
                       ));

                bundles.Add(new StyleBundle("~/Content/dashboard_global")
                    .Include("~/Content/css/bootstrap.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/css/font-awesome.min.css", new CssRewriteUrlTransform())
                    .Include("~/Content/components/msgGrowl/css/msgGrowl.css", new CssRewriteUrlTransform())
                    .Include(
                        "~/Content/css/my_dashboard.css",
                        "~/Content/css/demo.min.css",
                        "~/Content/css/animate.min.css",
                        "~/Content/components/zoomify/zoomify.css",
                        "~/Content/components/bootstrap-select/css/bootstrap-select.css",
                        "~/Content/css/beyond.min.css"
                    ));

            }

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}

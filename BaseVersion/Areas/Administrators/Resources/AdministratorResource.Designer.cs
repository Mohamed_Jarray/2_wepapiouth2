﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BaseVersion.Areas.Administrators.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AdministratorResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AdministratorResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("BaseVersion.Areas.Administrators.Resources.AdministratorResource", typeof(AdministratorResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to اضافة مدير.
        /// </summary>
        public static string AddAdministrator {
            get {
                return ResourceManager.GetString("AddAdministrator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to مدير.
        /// </summary>
        public static string Administrator {
            get {
                return ResourceManager.GetString("Administrator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to قائمة المديرين.
        /// </summary>
        public static string AdministratorList {
            get {
                return ResourceManager.GetString("AdministratorList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to المديرين.
        /// </summary>
        public static string Administrators {
            get {
                return ResourceManager.GetString("Administrators", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to لا يمكن حذف هذا المدير لأنك متصل به الآن.
        /// </summary>
        public static string CantDeleteAdmin {
            get {
                return ResourceManager.GetString("CantDeleteAdmin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to تعديل مدير.
        /// </summary>
        public static string EditAdministrator {
            get {
                return ResourceManager.GetString("EditAdministrator", resourceCulture);
            }
        }
    }
}

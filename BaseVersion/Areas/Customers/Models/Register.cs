﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BaseVersion.Areas.Customers.Models
{
    public class Register
    {
        [Required]
        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        [StringLength(15, MinimumLength = 5)]
        [RegularExpression(@"[0-9]+$")]
        [Required]
        public string Phone { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
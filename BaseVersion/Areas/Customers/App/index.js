﻿$("#Menu_Customer").addClass("active");

function insialise_customer_datatable(datatable_parameters) {
    jQuery(document).ready(function () {
        var oTable = jQuery(datatable_parameters.TableName).DataTable({
            "aoColumnDefs": [
                {
                    'aTargets': [0],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data === null)
                            return '&ndash;';
                        else
                            return '<div class="text-align-right">' + data + '</div>';
                    }
                },
                {
                    'aTargets': [1],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data === null)
                            return '&ndash;';
                        else
                            return '<div>' + data + '</div>';
                    }
                },
                {
                    'aTargets': [2],
                    'bSortable': true,
                    'mRender': function (data, type, full) {
                        if (data === null)
                            return '&ndash;';
                        else
                            return '<div>' + data + '</div>';
                    }
                },
                {
                    'aTargets': [3],
                    'bSortable': false,
                    'mRender': function (data, type, row) {
                        return '<div>'
                            + '<a href="' + datatable_parameters.DetailsUrl + '/' + data + '" class="btn btn-info btn-xs"><i class="fa fa-address-card"></i>' + datatable_parameters.Details + '</a>&nbsp;'
                            + '</div>';
                    }
                },
            ],

            "sAjaxSource": datatable_parameters.ActionUrl,
            "bServerSide": true,
            "bLengthChange": true,
            "iDisplayLength": 10,
            "bStateSave": true,
            "fnPreDrawCallback": function (oSettings) {
                $('.loading-container').removeClass('loading-inactive').addClass('loading-active');
            },
            "fnDrawCallback": function (oSettings) {
                $('.loading-container').removeClass('loading-active').addClass('loading-inactive');
            },
            "oLanguage": {
                "sLengthMenu": datatable_parameters.NumberRows + " _MENU_ ",
                "sInfo": datatable_parameters.Display + ' _START_ ' + datatable_parameters.To + ' _END_ ' + datatable_parameters.From + '_TOTAL_ ' + datatable_parameters.Rows,
                "sInfoEmpty": datatable_parameters.Display + ' 0 ' + datatable_parameters.To + ' _END_ ' + datatable_parameters.From + ' _TOTAL_ ' + datatable_parameters.Rows,
                "sZeroRecords": datatable_parameters.NoData,
                "sEmptyTable": datatable_parameters.NoData,
                "oPaginate": {
                    "sPrevious": '<i class="fa fa-angle-double-right"></i>',
                    "sNext": '<i class="fa fa-angle-double-left"></i>'
                }
            },
            "aLengthMenu": [[1, 2, 5, 10, 25, 50, 100], [1, 2, 5, 10, 25, 50, 100]]
        });

        jQuery('.dataTables_length select').select2({ minimumResultsForSearch: -1 });
    });
}

function filterTable(tableName) {
    var oTable = jQuery(tableName).dataTable();
    var oSettings = oTable.fnSettings();
    oSettings.aoPreSearchCols[0].sSearch = jQuery('#name').val();
    oSettings.aoPreSearchCols[1].sSearch = jQuery('#phone').val();
    oSettings.aoPreSearchCols[2].sSearch = jQuery('#email').val();
    oTable.fnDraw(false);
}

function clearfilterTable(tableName) {
    var oTable = jQuery(tableName).dataTable();
    var oSettings = oTable.fnSettings();
    for (iCol = 0; iCol < oSettings.aoPreSearchCols.length; iCol++) {
        oSettings.aoPreSearchCols[iCol].sSearch = '';
    }
    oTable.fnDraw(false);
    jQuery('#name').val("");
    jQuery('#phone').val("");
    jQuery('#email').val("");
}

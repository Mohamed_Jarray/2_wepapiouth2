﻿using BaseVersion.Areas.Customers.Models;
using BaseVersion.Helpers;
using BaseVersion.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using WebServices;

namespace BaseVersion.Areas.Customers.WebServices
{
    [RoutePrefix("api/Customer")]
    public class CustomerController : ApiController
    {
        private BaseVersionEntities db = new BaseVersionEntities();
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public CustomerController()
        {
        }

        public CustomerController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpPost]
        [Route("Create")]
        public async Task<IHttpActionResult> Create([FromBody]Register model)
        {
            ZedneyResult<ZedneyNonQuery> zedneyResult = new ZedneyResult<ZedneyNonQuery>();

            if (!ModelState.IsValid)
            {
                zedneyResult.ResultNum = WebServiceHelper.VALIDATION_ERROR_EXCEPTION;
                zedneyResult.ResultMessage = WebServiceHelper.VALIDATION_ERROR_MESSAGE;
                return Ok(zedneyResult);
            }

            try
            {
                var user = new ApplicationUser { UserName = model.Phone, Email = model.Email, Name = model.Name, PhoneNumber = model.Phone };
                var result = await UserManager.CreateAsync(user, model.Password);

                var roleresult = UserManager.AddToRole(user.Id, CustomHelper.CUSTOMER);

                zedneyResult.ResultNum = WebServiceHelper.SUCCESS_NUMBER;
                zedneyResult.ResultMessage = WebServiceHelper.SUCCESS_MESSAGE;
                return Ok(zedneyResult);
            }
            catch (Exception ex)
            {
                zedneyResult.ResultNum = WebServiceHelper.OTHER_EXCEPTION;
                zedneyResult.ResultMessage = CommonHelper.GetExceptionDetails(ex);
                return Ok(zedneyResult);
            }
        }

        [Authorize]
        [HttpPut]
        [Route("Edit")]
        public async Task<IHttpActionResult> Edit([FromBody]Register model)
        {
            ZedneyResult<ZedneyNonQuery> zedneyResult = new ZedneyResult<ZedneyNonQuery>();

            if (!ModelState.IsValid)
            {
                zedneyResult.ResultNum = WebServiceHelper.VALIDATION_ERROR_EXCEPTION;
                zedneyResult.ResultMessage = WebServiceHelper.VALIDATION_ERROR_MESSAGE;
                return Ok(zedneyResult);
            }

            try
            {
                IIdentity identiy = ControllerContext.RequestContext.Principal.Identity;
                ApplicationUser user = UserManager.FindById(identiy.GetUserId());
                user.Name = model.Name;
                user.PhoneNumber = model.Phone;
                user.UserName = model.Phone;
                user.Email = model.Email;
                UserManager.Update(user);
                if (!string.IsNullOrEmpty(model.Password))
                {
                    string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                    var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
                    if (result.Succeeded)
                    {
                        zedneyResult.ResultNum = WebServiceHelper.SUCCESS_NUMBER;
                        zedneyResult.ResultMessage = WebServiceHelper.SUCCESS_MESSAGE;
                        return Ok(zedneyResult);
                    }
                    else
                    {
                        zedneyResult.ResultNum = WebServiceHelper.OTHER_EXCEPTION;
                        zedneyResult.ResultMessage = result.Errors.FirstOrDefault();
                        return Ok(zedneyResult);
                    }
                }

                zedneyResult.ResultNum = WebServiceHelper.SUCCESS_NUMBER;
                zedneyResult.ResultMessage = WebServiceHelper.SUCCESS_MESSAGE;
                return Ok(zedneyResult);
            }
            catch (Exception ex)
            {
                zedneyResult.ResultNum = WebServiceHelper.OTHER_EXCEPTION;
                zedneyResult.ResultMessage = CommonHelper.GetExceptionDetails(ex);
                return Ok(zedneyResult);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("Profile")]
        public IHttpActionResult Profile()
        {
            ZedneyResult<ApplicationUser> zedneyResult = new ZedneyResult<ApplicationUser>();

            if (!ModelState.IsValid)
            {
                zedneyResult.ResultNum = WebServiceHelper.VALIDATION_ERROR_EXCEPTION;
                zedneyResult.ResultMessage = WebServiceHelper.VALIDATION_ERROR_MESSAGE;
                return Ok(zedneyResult);
            }

            try
            {
                IIdentity identiy = ControllerContext.RequestContext.Principal.Identity;
                ApplicationUser user = UserManager.FindById(identiy.GetUserId());

                zedneyResult.ResultObject.Add(user);
                zedneyResult.ResultNum = WebServiceHelper.SUCCESS_NUMBER;
                zedneyResult.ResultMessage = WebServiceHelper.SUCCESS_MESSAGE;
                return Ok(zedneyResult);
            }
            catch (Exception ex)
            {
                zedneyResult.ResultNum = WebServiceHelper.OTHER_EXCEPTION;
                zedneyResult.ResultMessage = CommonHelper.GetExceptionDetails(ex);
                return Ok(zedneyResult);
            }
        }
    }
}

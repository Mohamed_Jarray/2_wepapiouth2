﻿using BaseVersion.Controllers;
using BaseVersion.Helpers;
using BaseVersion.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BaseVersion.Areas.Customers.Controllers
{
    [CustomAuthorize(Roles = CustomHelper.SUPERADMIN + "," + CustomHelper.ADMINISTRATOR)]
    public class CustomerController : BaseController
    {
        private BaseVersionEntities db = new BaseVersionEntities();

        // GET: Customers/Customer
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetAjaxData()
        {
            #region Build Order by Clause And Pagination parameters
            Pagination pagination = new Pagination();
            pagination = this.DataTablePagination(pagination);
            #endregion

            int? count = 0;
            string name = null;
            string phone = null;
            string email = null;
            name = CommonHelper.PrepareFilterString(Request["sSearch_0"]);
            phone = CommonHelper.PrepareFilterString(Request["sSearch_1"]);
            email = CommonHelper.PrepareFilterString(Request["sSearch_2"]);

            List<spCustomerGetByPage_Result> customers = db.spCustomerGetByPage(name, email, phone, pagination.SortColIndex, pagination.OrderByColumnDir, pagination.PageNum, pagination.DisplayLength).ToList();

            if (customers.Count > 0)
            {
                count = customers.First().TotalRows;
            }
            var Data = customers.Select(d => new string[] {
                            d.Name,
                            d.PhoneNumber,
                            d.Email,
                            d.Id.ToString()}).ToArray();
            return Json(new
            {
                aaData = Data,
                iTotalDisplayRecords = count,
                iTotalRecords = count,
                sEcho = Request.Params["sEcho"]
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
﻿/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	
	// %REMOVE_START%
	// The configuration options below are needed when running CKEditor from source files.
    config.plugins = 'oembed,confighelper,audio,dialogui,dialog,a11yhelp,dialogadvtab,basicstyles,bidi,blockquote,clipboard,button,panelbutton,panel,floatpanel,colorbutton,colordialog,templates,menu,contextmenu,div,resize,toolbar,elementspath,enterkey,entities,popup,filebrowser,find,fakeobjects,flash,floatingspace,listblock,richcombo,font,format,horizontalrule,htmlwriter,wysiwygarea,image,indent,indentblock,indentlist,justify,menubutton,link,list,liststyle,magicline,maximize,newpage,pastetext,pastefromword,preview,print,removeformat,selectall,showblocks,showborders,specialchar,stylescombo,tab,table,tabletools,undo,wsc,slideshow,osem_googlemaps,tabbedimagebrowser,tableresize,imagebrowser,imgbrowse,imagepaste,lineutils,widget,image2,imageresponsive,zoom,youtube';
	config.skin = 'flat';
	// %REMOVE_END%
	// Define changes to default configuration here. For example:
	 config.language = 'ar';
    // config.uiColor = '#AADC6E';

	//config.toolbarGroups = [
    //{ name: 'clipboard', groups: ['clipboard', 'undo'] },
    //{ name: 'editing', groups: ['find', 'selection'] },
    //{ name: 'links' },
    //{ name: 'insert' },
    //{ name: 'forms' },
    //{ name: 'tools' },
    //{ name: 'document', groups: ['mode', 'document', 'doctools'] },
    //{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
    //{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'] },
    //{ name: 'styles' },
    //{ name: 'colors' },
    //{ name: 'others' }
	//];


	//config.filebrowserUploadUrl = '/uploader/upload.php';

};

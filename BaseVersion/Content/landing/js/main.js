document.onselectstart = function(){ return false; }; // disable page selection
document.oncontextmenu = function(){ return false; }; // disable page contextmenu
$(document).ready(function () {

    //Code to center the content div
    $box = $('.content');
    $ht = $box.height() + 175;
    $win_ht = $(window).height();

    if ($win_ht > $ht) {
        $box.css({
            'left': '50%',
            'top': '50%',
            'margin-left': -$box.width() / 2 + 'px',
            'margin-top': -$ht / 2 + 'px'
        });
    } else {
        $box.css({
            'left': '50%',
            'margin-left': -$box.width() / 2 + 'px',
            'margin-top': '60px',
            'margin-bottom': '10px'
        });
    }
    var baseurl = $("#body-bg").data("url");
    //code for the background slider
    $.backstretch([
        baseurl+'Content/landing/img/backslider/jameel1.jpg',
        baseurl+'Content/landing/img/backslider/jameel3.jpg',
        baseurl+'Content/landing/img/backslider/jameel4.jpg',
        baseurl+'Content/landing/img/backslider/jameel5.jpg',
        baseurl+'Content/landing/img/backslider/jameel6.jpg',
        baseurl+'Content/landing/img/backslider/jameel7.jpg',
        baseurl+'Content/landing/img/backslider/jameel8.jpg',
        baseurl+'Content/landing/img/backslider/jameel9.jpg'
    ], {
            fade: 750,
            duration: 4000
        });


    //code for the cirlces Countdouwn
    $(".counter").TimeCircles({
        "direction": "Clockwise",
        "animation": "smooth",
        "bg_width": 0,
        "count_past_zero": false,
        "fg_width": 0.03,
        "circle_bg_color": "rgba(255, 255, 255, 0)",
        "circle_bg_fill_color": "rgba(255, 255, 255, 0.1)",
        "time": {
            "Days": {
                "text": "يوم",
                "color": "#FFA500",
                "show": true
            },
            "Hours": {
                "text": "ساعة",
                "color": "#EE82EE",
                "show": true
            },
            "Minutes": {
                "text": "دقيقة",
                "color": "#9ACD32",
                "show": true
            },
            "Seconds": {
                "text": "ثانية",
                "color": "#6495ED",
                "show": true
            }
        }
    });
    $(".counter").TimeCircles().addListener(function (unit, value, total) {
        if (total <= 0) location.reload(true); //call the Index in Homecontroller
    });
    $(window).on('resize', function () {
        $('#counter').TimeCircles().rebuild();
    });
});

﻿
namespace BaseVersion.Models
{
    public class Pagination
    {
        public Pagination()
        {
            // TODO: Complete member initialization
            this.DisplayLength = 10;
            this.PageNum = 1;
            this.SortColIndex = 0;
            this.OrderByColumnDir = "asc";
        }

        public Pagination(int displayLength, int pageNum, int sortColIndex, string orderByColumnDir)
        {
            // TODO: Complete member initialization
            this.DisplayLength = displayLength;
            this.PageNum = pageNum;
            this.SortColIndex = sortColIndex;
            this.OrderByColumnDir = orderByColumnDir;
        }
        public int DisplayLength { get; set; }
        public int PageNum { get; set; }
        public int SortColIndex { get; set; }
        public string OrderByColumnDir { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace WebServices
{
    /// <summary>
    /// 
    /// </summary>
    public class ZedneyResult<T>
    {
        public List<T> ResultObject{ get; set; }
        public int ResultNum { get; set; }
        public string ResultMessage { get; set; }

        public ZedneyResult()
        {
            ResultObject = new List<T>();
        }
       
    }
}
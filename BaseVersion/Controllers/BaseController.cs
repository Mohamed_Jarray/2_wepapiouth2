﻿using BaseVersion.Helpers;
using BaseVersion.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace BaseVersion.Controllers
{
    public class BaseController : Controller
    {       

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string cultureName = null;

            // Attempt to read the culture cookie from Request
            HttpCookie cultureCookie = Request.Cookies["_culture"];
            if (cultureCookie != null)
                cultureName = cultureCookie.Value;
            else
                cultureName = Request.UserLanguages != null && Request.UserLanguages.Length > 0 ?
                        Request.UserLanguages[0] :  // obtain it from HTTP header AcceptLanguages
                        null;
            // Validate culture name
            cultureName = CultureHelper.GetImplementedCulture(cultureName); // This is safe

            // Modify current thread's cultures            
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en");
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureName);

            return base.BeginExecuteCore(callback, state);
        }

        public void ShowGrowlMessage(string message, string type)
        {
            TempData.Remove("Msg");
            TempData.Remove("MsgType");
            TempData.Add("Msg", message);
            TempData.Add("MsgType", type);
        }

        public Pagination DataTablePagination(Pagination pagination)
        {
            int displayStart = 1;
            if (Request.Params["sEcho"] != null)
            {
                displayStart = Int32.Parse(Request.Params["iDisplayStart"]);
                pagination.DisplayLength = Int32.Parse(Request.Params["iDisplayLength"]);
                pagination.PageNum = (int)Math.Ceiling((double)displayStart / pagination.DisplayLength) + 1;

                int.TryParse(Request.Params["iSortingCols"], out int totalSortingCols);

                for (int i = 0; i < totalSortingCols; i++)
                {
                    pagination.SortColIndex = Int32.Parse(Request.Params[String.Format("iSortCol_{0}", i)]);

                    pagination.OrderByColumnDir = Request.Params[String.Format("sSortDir_{0}", i)];
                }
            }
            return pagination;
        }
    }
}
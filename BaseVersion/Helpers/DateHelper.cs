﻿using System;
using System.Globalization;
using BaseVersion.Resources;


namespace BaseVersion.Helpers
{
    public class DateHelper
    {
        public static string FormatDate(DateTime date)
        {
            return string.Format("{0:yyyy/MM/dd HH:mm}", date);
        }

        public static string FormatShortDate(DateTime date)
        {
            return string.Format("{0:yyyy/MM/dd}", date);
        }


        public static DateTime ToSaudiDateTime()
        {
            TimeZone localZone = TimeZone.CurrentTimeZone;
            return localZone.ToUniversalTime(DateTime.Now).AddHours(3);
        }


        public static DateTime ToSaudiDateTime(DateTime sourceDateTime)
        {
            TimeZone localZone = TimeZone.CurrentTimeZone;
            return localZone.ToUniversalTime(sourceDateTime).AddHours(3);
        }


        public static string GregToHijri(string date)
        {
            CultureInfo arCI = new CultureInfo("ar-SA");
            CultureInfo enCI = new CultureInfo("en-US");

            DateTime tempDate = DateTime.Parse(date, CultureInfo.InvariantCulture);
            return tempDate.ToString("yyyy/MM/dd", arCI.DateTimeFormat);
        }

        public static string GregToHijriWithTime(string date)
        {
            CultureInfo arCI = new CultureInfo("ar-SA");
            CultureInfo enCI = new CultureInfo("en-US");

            DateTime tempDate = DateTime.Parse(date, CultureInfo.InvariantCulture);
            return tempDate.ToString("yyyy/MM/dd HH:mm:ss", arCI.DateTimeFormat);
        }

        public static string HijriToGreg(string date)
        {
            CultureInfo arCI = new CultureInfo("ar-SA");
            CultureInfo enCI = new CultureInfo("en-US");

            DateTime tempDate = DateTime.ParseExact(date, "yyyy/MM/dd", arCI.DateTimeFormat, DateTimeStyles.AllowWhiteSpaces);
            return tempDate.ToString("MM/dd/yyyy", enCI.DateTimeFormat);
        }

        public static string HijriToGregWithTime(string date)
        {
            CultureInfo arCI = new CultureInfo("ar-SA");
            CultureInfo enCI = new CultureInfo("en-US");

            DateTime tempDate = DateTime.ParseExact(date, "yyyy/MM/dd HH:mm:ss", arCI.DateTimeFormat, DateTimeStyles.AllowWhiteSpaces);
            return tempDate.ToString("MM/dd/yyyy HH:mm:ss", enCI.DateTimeFormat);
        }

        public static string ConvertHoursToTotalDays(int hours) 
        {
            int NumberOfdays = 0;
            int NumberOfhours = 0;
            NumberOfdays= hours/24;
            NumberOfhours = hours % 24;
            if (NumberOfdays == 0)
            {
                return String.Format("{0} " + Resource.Hour, NumberOfhours);
            }
            else
            {
                return String.Format("{0} " + Resource.Day + " " + Resource.And + " {1} " + Resource.Hour, NumberOfdays, NumberOfhours);
            }
        }

    }
}
﻿using System.Text;
using System.Web;

namespace BaseVersion.Helpers
{
    public class EvaluationHelper
    {
        public static IHtmlString Show(double? evaluation)
        {
            StringBuilder result = new StringBuilder();

            for(int counter = 1; counter <= 5; counter++)
            {
                if (evaluation == null)
                    result.Append("<span class='fa fa-star-o gold'></span>");
                else if (counter < evaluation)
                    result.Append("<span class='fa fa-star gold'></span>");
                else if(counter -1 < evaluation)
                    result.Append("<span class='fa fa-star-half-o fa-flip-horizontal gold'></span>");
                else
                    result.Append("<span class='fa fa-star-o gold'></span>");
            }
           
            return new HtmlString(result.ToString());
        }
    }
}
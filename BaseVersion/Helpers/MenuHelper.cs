﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Text;
using BaseVersion.Models;

namespace BaseVersion.Helpers
{
    public class MenuHelper
    {
        public static IHtmlString AddItem(string icon, string action, string controller, string area, string manyLabel)
        {
            var requestContext = HttpContext.Current.Request.RequestContext;
            var urlList = new UrlHelper(requestContext).Action(action, controller, new { area = area });

            string itemMenu = @"<li id='Menu_##NAVREFERENCE##'>
                                <a href='##URL##' class='menu-dropdown'>
                                    <i class='menu-icon fa fa-##ICON##'></i>
                                    <span class='menu-text'>##MAINLABEL##</span>
                                </a>
                                </li>";

            itemMenu = itemMenu.Replace("##NAVREFERENCE##", controller)
                               .Replace("##URL##", urlList)
                               .Replace("##ICON##", icon)
                               .Replace("##MAINLABEL##", manyLabel);

            return new HtmlString(itemMenu);
        }

        /// <summary>
        /// surchage
        /// </summary>
        /// <param name="icon"></param>
        /// <param name="action"></param>
        /// <param name="controller"></param>
        /// <param name="area"></param>
        /// <param name="manyLabel"></param>
        /// <param name="listlabel"></param>
        /// <param name="createLabel"></param>
        /// <returns></returns>
        public static IHtmlString AddItem(string icon, string action, string controller, string area, string manyLabel, string listlabel, string createLabel)
        {
            var requestContext = HttpContext.Current.Request.RequestContext;
            var urlList = new UrlHelper(requestContext).Action(action, controller, new { area = area });
            var urlCreate = new UrlHelper(requestContext).Action("Create", controller, new { area = area });
            var subMenu = @"<ul class='submenu'>
                                    <li>
                                        <a href='##URL##'>
                                            <span class='menu-text'>##SUBLABEL##</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href='##URLCREATE##'>
                                            <span class='menu-text'>##CREATELABEL##</span>
                                        </a>
                                    </li>
                                </ul>";
            string itemMenu = @"<li id='Menu_##NAVREFERENCE##'>
                                <a href='javascript:;' class='menu-dropdown'>
                                    <i class='menu-icon fa fa-##ICON##'></i>
                                    <span class='menu-text'>##MAINLABEL##</span>
                                     <i class='menu-expand'></i>
                                </a>
                                ##SUBMENU##
                                </li>";

            subMenu = subMenu.Replace("##URL##", urlList)
                             .Replace("##SUBLABEL##", listlabel)
                             .Replace("##URLCREATE##", urlCreate)
                             .Replace("##CREATELABEL##", createLabel);

            itemMenu = itemMenu.Replace("##NAVREFERENCE##", controller)
                               .Replace("##ICON##", icon)
                               .Replace("##MAINLABEL##", manyLabel)
                               .Replace("##SUBMENU##", subMenu);


            return new HtmlString(itemMenu);
        }

    }
}
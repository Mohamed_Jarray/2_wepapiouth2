﻿using System;
using System.Security.Cryptography;
using System.Text;


namespace BaseVersion.Helpers
{
    public class KeyHelper
    {
        public static string GetAlphaNumericUniqueKey(int givenSize)
        {
            int maxSize = 9;
            //int minSize = 5;
            char[] chars = new char[62];
            string a;
            a = "1234567890ABCDEFGHIGKLMNOPKRSTUVQWXYZ";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = givenSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public static string getRandomCode()
        {
            string registerNumber = "";
            Random random = new Random();
            for (var i = 0; i < 6; i++)
            {
                int number = random.Next(0, 10);

                registerNumber = registerNumber + number.ToString();
            }
            return registerNumber;
        }

        public static string GetUniqueKey(int givenSize)
        {
            int maxSize = 9;
            //int minSize = 5;
            char[] chars = new char[62];
            string a;
            a = "1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = givenSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

    }
}
﻿using System.Web;

namespace BaseVersion.Helpers
{
    public class LoaderHelper
    {
        public static IHtmlString Show(string className)
        {
            string loader = @"<div id='loader-wrapper' class='##CLASSNAME##'>
                                  <div id='loader'></div > 
                                  <div class='loader-section section-left'></div>
                                  <div class='loader-section section-right'></div>
                                </div>";

            loader = loader.Replace("##CLASSNAME##", className);

            return new HtmlString(loader);
        }
    }
}
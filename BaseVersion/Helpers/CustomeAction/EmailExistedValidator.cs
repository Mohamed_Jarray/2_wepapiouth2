﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using BaseVersion.Models;
using BaseVersion.Resources;

namespace BaseVersion.Helpers
{
    public class EmailExistedAttribute : ValidationAttribute
    {
        private BaseVersionEntities db = new BaseVersionEntities();
        // annotation to validate email unicity: email must be in table 'UserProfile'
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var containerType = validationContext.ObjectInstance.GetType();
            var field = containerType.GetProperty("UserId");
            string userId= field.GetValue(validationContext.ObjectInstance, null).ToString();
            string email = (string)value;
            bool isValid = (db.AspNetUsers.Where(i => i.Email == email && i.Id != userId && email != null && email != string.Empty).Count() == 0);

            if (isValid)
                return ValidationResult.Success;
            else
                return new ValidationResult(Resource.EmailUsed);
        }
    }
}
﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Hosting;


namespace BaseVersion.Helpers
{
    public class EmailHelper
    {
        public static void SendEmail(string destination, string subject, string body)
        {
            try
            {
                MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["EmailSender"], destination, subject, body)
                {
                    BodyEncoding = System.Text.Encoding.GetEncoding("utf-8"),
                    IsBodyHtml = true,
                    DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
                };
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"])
                {
                    UseDefaultCredentials = false,
                    Port = int.Parse(ConfigurationManager.AppSettings["PortServer"]),
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPLogin"], ConfigurationManager.AppSettings["SMTPPassword"])
                };
                SmtpServer.Send(mail);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.InnerException);
            }

        }

        public static void SendEmailTemplate(string destination, string subject, string username, string applicationName, string slogan, string resetLink)
        {
            try
            {
                MailMessage mail = new MailMessage
                {
                    BodyEncoding = System.Text.Encoding.GetEncoding("utf-8"),
                    From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"])
                };
                mail.To.Add(destination);
                StreamReader reader = null;
                reader = new StreamReader(HostingEnvironment.MapPath("~/Views/Shared/EmailTemplates/EmailTemplate.html"));

                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                //Here replace the key string
                StrContent = StrContent.Replace("[ApplicationName]", applicationName);
                StrContent = StrContent.Replace("[Slogan]", slogan);
                StrContent = StrContent.Replace("[Username]", username);
                StrContent = StrContent.Replace("[ResetLink]", resetLink);
                mail.Subject = subject;
                mail.Body = StrContent.ToString();
                mail.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"])
                {
                    UseDefaultCredentials = false,
                    Port = int.Parse(ConfigurationManager.AppSettings["PortServer"]),
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPLogin"], ConfigurationManager.AppSettings["SMTPPassword"])
                };
                SmtpServer.Send(mail);
            }
            catch (Exception)
            { }
        }

        public static void SendEmailResetPassword(string username, string code, string email)
        {
            // Email stuff
            string subject = " تغيير كلمة المرور في تطبيق   " + ConfigurationManager.AppSettings["WebSiteName"];
            string msg = @"<html xmlns='http://www.w3.org/1999/xhtml'>
                                   <body dir='rtl'>
                                        <table style='width: 500px; border: 1px solid #035CA7;margin-right: 20%;'>
                                            <tr style='height:40px'>
                                                <div style='background-color: #007BC9; font-size: small; font-weight: bold; color: #FFFFFF;width:20%;float: right;padding: 23px;'><img id=':0_28-e' name=':0' src='https://ssl.gstatic.com/ui/v1/icons/mail/profile_mask2.png' class='ajn' style='background-color: #cccccc;border-radius: 16px;' jid='contact@centrah.com' data-name='' aria-hidden='true'></div>
                                                <div style='background-color: #007BC9; font-size: small; font-weight: bold; color: #FFFFFF;font-size: 22px;padding: 28px;'>تطبيق " + ConfigurationManager.AppSettings["WebSiteName"] + @"</div>
                                            </tr>
                                            <tr>
                                                <td style='font-family: Arial, Helvetica, sans-serif; font-size: 17px;    padding-right: 20px;color:#262626'>
                                                    <br /><b style='line-height: 24px;'>##BODY##<b><br /><br /><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </body>
                                    </html>";


            string body = "السلام عليكم ورحمة الله وبركاته،<br /><br />لقد قمت بطلب تغيير كلمة المرور.<br />"
                              + "إسم المستخدم: <b>" + username + "</b><br />"
                              + "كود تغيير كلمة المرور : "
                              + code
                              + "<br /><br />"
                              + "إذا لم تقم بالطلب فنرجو تجاهل هذه الرسالة.";
            msg = msg.Replace("##BODY##", body);
            string from = email;
            MailMessage mail = new MailMessage(ConfigurationManager.AppSettings["EmailSender"], from, subject, msg)
            {
                BodyEncoding = System.Text.Encoding.GetEncoding("utf-8"),
                IsBodyHtml = true
            };
            SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"])
            {
                Port = int.Parse(ConfigurationManager.AppSettings["PortServer"]),
                Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPLogin"], ConfigurationManager.AppSettings["SMTPPassword"])
            };
            SmtpServer.Send(mail);
        }

        public static async Task SendEmailResetPasswordWithURL(string username, string destination, string os_name, string browser, string resetLink)
        {

            StreamReader reader = new StreamReader(HostingEnvironment.MapPath("~/Views/Shared/EmailTemplates/ResetPassword.html"));
            string readFile = reader.ReadToEnd();
            string StrContent = "";
            StrContent = readFile;
            //Here replace the key string
            StrContent = StrContent.Replace("[APP_NAME]", "");
            StrContent = StrContent.Replace("[OS]", os_name);
            StrContent = StrContent.Replace("[BROWSER]", browser);
            StrContent = StrContent.Replace("[USER_NAME]", username);
            StrContent = StrContent.Replace("[ACTION_URL]", resetLink);

            MailMessage mail = new MailMessage
            {
                BodyEncoding = System.Text.Encoding.GetEncoding("utf-8"),
                From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"]),
                Subject = "تغيير كلمة المرور",
                Body = StrContent.ToString(),
                IsBodyHtml = true
            };
            mail.To.Add(destination);

            // your remote SMTP server IP.
            SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"])
            {
                UseDefaultCredentials = false,
                Port = int.Parse(ConfigurationManager.AppSettings["PortServer"]),
                Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPLogin"], ConfigurationManager.AppSettings["SMTPPassword"])
            };

            await SmtpServer.SendMailAsync(mail);

        }

        public static void SendEmailResetPasswordWithCodeReset(string username, string destination, string resetCode)
        {
            try
            {
                MailMessage mail = new MailMessage
                {
                    BodyEncoding = System.Text.Encoding.GetEncoding("utf-8"),
                    From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"])
                };
                mail.To.Add(destination);
                StreamReader reader = null;
                reader = new StreamReader(HostingEnvironment.MapPath("~/Views/Shared/EmailTemplates/ResetPasswordFromMobileApp.html"));
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                //Here replace the key string
                StrContent = StrContent.Replace("[APP_NAME]", ConfigurationManager.AppSettings["WebSiteName"]);
                StrContent = StrContent.Replace("[USER_NAME]", username);
                StrContent = StrContent.Replace("[CODE_RESET_PASSWORD]", resetCode);
                mail.Subject = "تغيير كلمة المرور";
                mail.Body = StrContent.ToString();
                mail.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"])
                {
                    UseDefaultCredentials = false,
                    Port = int.Parse(ConfigurationManager.AppSettings["PortServer"]),
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPLogin"], ConfigurationManager.AppSettings["SMTPPassword"])
                };
                SmtpServer.Send(mail);
            }
            catch (Exception)
            { }
        }

        public static void SendEmailActivation(string username, string destination, string code)
        {
            try
            {
                MailMessage mail = new MailMessage
                {
                    BodyEncoding = System.Text.Encoding.GetEncoding("utf-8"),
                    From = new MailAddress(ConfigurationManager.AppSettings["EmailSender"])
                };
                mail.To.Add(destination);
                StreamReader reader = null;
                reader = new StreamReader(HostingEnvironment.MapPath("~/Views/Shared/EmailTemplates/Activation.html"));
                string readFile = reader.ReadToEnd();
                string StrContent = "";
                StrContent = readFile;
                //Here replace the key string
                StrContent = StrContent.Replace("[APP_NAME]", ConfigurationManager.AppSettings["WebSiteName"]);
                StrContent = StrContent.Replace("[USER_NAME]", username);
                StrContent = StrContent.Replace("[CODE_ACTIVATION]", code);
                mail.Subject = "تفعيل الحساب";
                mail.Body = StrContent.ToString();
                mail.IsBodyHtml = true;
                // your remote SMTP server IP.
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"])
                {
                    UseDefaultCredentials = false,
                    Port = int.Parse(ConfigurationManager.AppSettings["PortServer"]),
                    Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPLogin"], ConfigurationManager.AppSettings["SMTPPassword"])
                };
                SmtpServer.Send(mail);
            }
            catch (Exception)
            { }
        }
    }
}